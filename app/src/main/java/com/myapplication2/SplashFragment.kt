package com.myapplication2

import android.os.Bundle
import android.os.CountDownTimer
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.navigation.NavController
import androidx.navigation.Navigation

class SplashFragment : Fragment(){

    var navController: NavController? = null



    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_splash, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)

        val timer = object: CountDownTimer(2000, 2000) {
            override fun onFinish() {
                navController!!.navigate(R.id.action_splashFragment_to_homeFragment)
            }
            override fun onTick(p0: Long) {

            }
        }

        timer.start()
    }

}
